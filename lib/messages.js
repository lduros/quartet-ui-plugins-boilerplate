"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const defaultLocaleMsg = {
  boilerplate: {
    navItemsTitle: "Boilerplate",
    menuLink1: "A Menu Link",
    subNavItem: "Another Item",
    developerInfo: "Developer Info"
  }
};

const french = {
  navItemsTitle: "Boilerplate",
  menuLink1: "Lien de menu",
  subNavItem: "Autre objet",
  developerInfo: "Info pour developpeurs"
};

exports.default = {
  "en-US": { plugins: _extends({}, defaultLocaleMsg) },
  "fr-FR": {
    plugins: {
      boilerplate: _extends({}, defaultLocaleMsg.boilerplate, french)
    }
  }
};