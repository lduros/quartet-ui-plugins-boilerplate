"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
// Copyright (c) 2018 SerialLab Corp.
//
// GNU GENERAL PUBLIC LICENSE
//    Version 3, 29 June 2007
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

const React = qu4rtet.require("react");

const Component = React.Component;

var _qu4rtet$require = qu4rtet.require("@blueprintjs/core");

const Menu = _qu4rtet$require.Menu,
      MenuItem = _qu4rtet$require.MenuItem,
      MenuDivider = _qu4rtet$require.MenuDivider,
      Dialog = _qu4rtet$require.Dialog,
      Button = _qu4rtet$require.Button,
      ButtonGroup = _qu4rtet$require.ButtonGroup,
      ContextMenu = _qu4rtet$require.ContextMenu;

var _qu4rtet$require2 = qu4rtet.require("./components/layouts/elements/TreeNode");

const TreeNode = _qu4rtet$require2.TreeNode;

var _qu4rtet$require3 = qu4rtet.require("react-router");

const withRouter = _qu4rtet$require3.withRouter;

var _qu4rtet$require4 = qu4rtet.require("react-redux");

const connect = _qu4rtet$require4.connect;

var _qu4rtet$require5 = qu4rtet.require("react-intl");

const FormattedMessage = _qu4rtet$require5.FormattedMessage;

var _qu4rtet$require6 = qu4rtet.require("./plugins/pluginRegistration");

const pluginRegistry = _qu4rtet$require6.pluginRegistry;


class SubMenu extends Component {
  render() {
    return React.createElement(
      TreeNode,
      {
        serverID: this.props.serverID,
        nodeType: "thumbup",
        depth: this.props.depth,
        childrenNodes: [] },
      React.createElement(FormattedMessage, { id: "plugins.boilerplate.subNavItem" })
    );
  }
}

class _NavRoot extends Component {
  constructor(...args) {
    var _temp;

    return _temp = super(...args), this.goTo = path => {
      // logic can be added here for link clicks.
      this.props.history.push(path);
    }, this.renderContextMenu = () => {
      // The Context menu is displayed on right click.
      const server = this.props.server;

      return React.createElement(
        Menu,
        null,
        React.createElement(MenuDivider, { title: server.serverSettingName }),
        React.createElement(MenuDivider, null),
        React.createElement(MenuItem, {
          text: pluginRegistry.getIntl().formatMessage({
            id: "plugins.boilerplate.menuLink1"
          })
        })
      );
    }, _temp;
  }

  // This static method is important
  // for the component to be properly registered
  static get PLUGIN_COMPONENT_NAME() {
    return "BoilerPlateNavRoot";
  }

  serverHasService() {
    // you may check here a service is available in the app list.
    // example to check if the output QU4RTET backend module is enabled:
    // return pluginRegistry
    //  .getServer(this.props.serverID)
    //  .appList.includes("output");

    // for now we just return true.
    return true;
  }

  render() {
    var _props = this.props;
    const serverID = _props.serverID,
          server = _props.server,
          history = _props.history;


    if (server && this.serverHasService()) {
      return React.createElement(
        TreeNode,
        {
          serverID: serverID,
          nodeType: "boilerplate",
          path: `/boilerplate/${serverID}/example-screen`,
          onContextMenu: this.renderContextMenu,
          depth: this.props.depth,
          childrenNodes: [React.createElement(SubMenu, this.props)] },
        React.createElement(FormattedMessage, { id: "plugins.boilerplate.navItemsTitle" })
      );
    } else {
      return null;
    }
  }
}

exports._NavRoot = _NavRoot;
const NavRoot = exports.NavRoot = connect((state, ownProps) => {
  return {
    server: state.serversettings.servers[ownProps.serverID],
    currentPath: state.layout.currentPath
  };
}, {})(withRouter(_NavRoot));